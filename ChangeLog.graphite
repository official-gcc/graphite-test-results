2010-06-07  Tobias Grosser  <grosser@fim.uni-passau.de>

	* Merge from mainline (159538:160224)

2010-05-19  Tobias Grosser  <grosser@fim.uni-passau.de>

	* Merge from mainline (159136:159536)

2010-05-05  Sebastian Pop  <sebastian.pop@amd.com>

	* Merge from mainline (158021:159135).

2010-04-16  Sebastian Pop  <sebastian.pop@amd.com>

	* configure.ac: Allow all the versions greater than 0.10 of PPL.
	* configure: Regenerated.

2010-04-06  Sebastian Pop  <sebastian.pop@amd.com>

	* Merge from mainline (157519:158021).

2010-03-17  Sebastian Pop  <sebastian.pop@amd.com>

	* Merge from mainline (157009:157519).

2010-02-23  Tobias Grosser  <grosser@fim.uni-passau.de>

	* Merge from mainline (156695:157009)

2010-02-13  Tobias Grosser  <grosser@fim.uni-passau.de>

	* Merge from mainline (154736:156693)

2009-11-09  Sebastian Pop  <sebpop@gmail.com>

	* configure.ac: Check for version 0.15.5 or later revision of CLooG.
	* configure: Regenerated.

2009-07-24  Tobias Grosser  <grosser@fim.uni-passau.de>

	* Makefile.def: Fix formatting to match trunk.

2009-06-24  Sebastian Pop  <sebastian.pop@amd.com>

	* contrib/automatic_tester/run_build.sh: Change the subject line
	to contain [graphite].
	* automatic_tester/graphite_env.sh.example: Added the gcc-testresults@
	list.

2009-06-05  Jan Sjodin  <jan.sjodin@amd.com>

	* Makefile.def: Make libpcp dependent on cloog.
	* Makefile.in: Generated.

2009-02-12  Jan Sjodin  <jan.sjodin@amd.com>

	* configure.ac: Add libpcp.
	* libpcp: New directory.
	* Makefile.def: Add libpcp.

2008-08-20  Sebastian Pop  <sebastian.pop@amd.com>

	* configure.ac: Request PPL version 0.10.
	* configure: Regenerated.

2008-08-13  Tobias Grosser  <grosser@fim.uni-passau.de>

	* configure: Rebuilt.
	* configure.ac: Improve PPL and CLooG to work also on FreeBSD.
	* gcc/Makefile.in: Add GMPLIBS to LIBS so that gcc detects them on
	FreeBSD.

2008-08-04  Sebastian Pop  <sebastian.pop@amd.com>

	* configure.ac: Check for CLooG and PPL version numbers.
	* configure: Regenerated.

2008-08-04  Sebastian Pop  <sebastian.pop@amd.com>

	* Revert patch from 2008-08-03.

2008-08-03  Sebastian Pop  <sebastian.pop@amd.com>

	* Makefile.def: Remove support for building ppl and cloog
	libs in gcc's sources.
	* Makefile.in: Regenerated.

2008-07-24  Sebastian Pop  <sebastian.pop@amd.com>

	* Merge from mainline (135673:138072).
	Reverted the MIRO changes (from 2008-04-05) that are now
	tracked in the miro branch.

2008-04-28  Sebastian Pop  <sebastian.pop@amd.com>

	* cloog: Removed.
	* polylib: Removed.

2008-04-07  Sebastian Pop  <sebastian.pop@amd.com>

	* polylib/source/arith/arithmetique.h (value_get_si): New.

2008-04-05  Alexander Lamaison  <awl03@doc.ic.ac.uk>
            Sebastian Pop  <sebastian.pop@amd.com>

	* Makefile.in: Add support for libbounds.
	* configure.ac: Same.
	* Makefile.def: Same.
	* configure: Regenerated.
	* libbounds: New.
	* libbounds/bounds-impl.h: New.
	* libbounds/configure: New.
	* libbounds/Makefile.in: New.
	* libbounds/libtool-version: New.
	* libbounds/bounds-oob.c: New.
	* libbounds/testsuite/boundsconfig.exp.in: New.
	* libbounds/testsuite/libbounds.cpp/pass37-pointer-subtract-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass36-pointer-add-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass20-pointer-addition-init1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass38-pointer-add-zero.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail16-pointer-decrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass28-pointer-addition-from-var-left.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail23-pointer-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail20-pointer-addition-init1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass14-pointer-subtraction2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail04-stack3darray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail12-pointer-subtraction1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass41-struct-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail32-pointer-address-addition.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail28-pointer-addition-from-var-left.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass62-pushback.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail26-pointer-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail19-pointer-predecrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass50-class.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass25-pointer-difference.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass52-class-string.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail44-struct-nested.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass05-stackarray-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass18-pointer-preincrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail35-function-params.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass27-pointer-addition-from-var-right.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail61-vector-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass51-class-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass46-struct-string-align.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass22-pointer-addition-from-array-init.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass16-pointer-decrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass34-pointer-difference-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass31-pointer-address-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail33-pointer-address-addition-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass54-class-nested.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail36-pointer-add-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail03-stack2darray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail21-pointer-subtraction-init1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass30-pointer-address.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass02-stackarray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail24-pointer-referent-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail02-stackarray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/cfrags.exp: New.
	* libbounds/testsuite/libbounds.cpp/pass44-struct-nested.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass01-noop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail15-pointer-increment.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass29-pointer-subtraction-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass63-stack.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail05-stackarray-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass62-pushback.s: New.
	* libbounds/testsuite/libbounds.cpp/pass33-pointer-address-addition-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail27-pointer-addition-from-var-right.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass23-pointer-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass17-pointer-addition-from-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail06-stackarray-referent-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail46-struct-string-align.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass04-stack3darray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass12-pointer-subtraction1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass21-pointer-subtraction-init1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass32-pointer-address-addition.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail37-pointer-subtract-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail11-pointer-addition1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail42-struct-string.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail13-pointer-addition2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass24-pointer-referent-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass19-pointer-predecrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail14-pointer-subtraction2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass15-pointer-increment.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass60-vector.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail30-pointer-address.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass56-class-string-align.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail60-vector.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail56-class-string-align.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass10-pointer.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail52-class-string.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail18-pointer-preincrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass40-struct.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass11-pointer-addition1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass03-stack2darray.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass42-struct-string.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass13-pointer-addition2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail22-pointer-addition-from-array-init.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail31-pointer-address-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail54-class-nested.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail17-pointer-addition-from-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass26-pointer-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass35-function-params.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass61-vector-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail29-pointer-subtraction-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/fail10-pointer.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass23-pointer-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass17-pointer-addition-from-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass12-pointer-subtraction1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail16-pointer-decrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass32-pointer-address-addition.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail11-pointer-addition1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass28-pointer-addition-from-var-left.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail23-pointer-referent.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass14-pointer-subtraction2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail12-pointer-subtraction1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail13-pointer-addition2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass24-pointer-referent-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass19-pointer-predecrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail32-pointer-address-addition.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail14-pointer-subtraction2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail28-pointer-addition-from-var-left.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass60-vector.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass15-pointer-increment.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail19-pointer-predecrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail26-pointer-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail30-pointer-address.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail60-vector.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass25-pointer-difference.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass10-pointer.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass18-pointer-preincrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass27-pointer-addition-from-var-right.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail61-vector-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail18-pointer-preincrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass16-pointer-decrement.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass11-pointer-addition1.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass34-pointer-difference-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass31-pointer-address-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail33-pointer-address-addition-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass13-pointer-addition2.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail31-pointer-address-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail17-pointer-addition-from-array.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass26-pointer-oob.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass30-pointer-address.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail24-pointer-referent-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail15-pointer-increment.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass29-pointer-subtraction-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass61-vector-loop.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail29-pointer-subtraction-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail10-pointer.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/pass33-pointer-address-addition-from-var.cpp: New.
	* libbounds/testsuite/libbounds.cpp/heap/fail27-pointer-addition-from-var-right.cpp: New.
	* libbounds/testsuite/libbounds.cpp/pass06-stackarray-referent-loop.cpp: New.
	* libbounds/testsuite/Makefile.in: New.
	* libbounds/testsuite/libbounds.c/pass22-pointer-addition-from-array-init.c: New.
	* libbounds/testsuite/libbounds.c/pass16-pointer-decrement.c: New.
	* libbounds/testsuite/libbounds.c/pass11-pointer-addition1.c: New.
	* libbounds/testsuite/libbounds.c/pass42-struct-string.c: New.
	* libbounds/testsuite/libbounds.c/fail22-pointer-addition-from-array-init.c: New.
	* libbounds/testsuite/libbounds.c/fail36-pointer-add-oob.c: New.
	* libbounds/testsuite/libbounds.c/fail21-pointer-subtraction-init1.c: New.
	* libbounds/testsuite/libbounds.c/fail02-stackarray.c: New.
	* libbounds/testsuite/libbounds.c/pass29-pointer-subtraction-from-var.c: New.
	* libbounds/testsuite/libbounds.c/fail15-pointer-increment.c: New.
	* libbounds/testsuite/libbounds.c/pass35-function-params.c: New.
	* libbounds/testsuite/libbounds.c/fail29-pointer-subtraction-from-var.c: New.
	* libbounds/testsuite/libbounds.c/fail10-pointer.c: New.
	* libbounds/testsuite/libbounds.c/pass33-pointer-address-addition-from-var.c: New.
	* libbounds/testsuite/libbounds.c/fail27-pointer-addition-from-var-right.c: New.
	* libbounds/testsuite/libbounds.c/fail02-stackarray.s: New.
	* libbounds/testsuite/libbounds.c/pass37-pointer-subtract-oob.c: New.
	* libbounds/testsuite/libbounds.c/pass36-pointer-add-oob.c: New.
	* libbounds/testsuite/libbounds.c/pass23-pointer-referent.c: New.
	* libbounds/testsuite/libbounds.c/pass38-pointer-add-zero.c: New.
	* libbounds/testsuite/libbounds.c/pass04-stack3darray.c: New.
	* libbounds/testsuite/libbounds.c/fail46-struct-string-align.c: New.
	* libbounds/testsuite/libbounds.c/pass12-pointer-subtraction1.c: New.
	* libbounds/testsuite/libbounds.c/fail16-pointer-decrement.c: New.
	* libbounds/testsuite/libbounds.c/pass32-pointer-address-addition.c: New.
	* libbounds/testsuite/libbounds.c/fail37-pointer-subtract-oob.c: New.
	* libbounds/testsuite/libbounds.c/fail11-pointer-addition1.c: New.
	* libbounds/testsuite/libbounds.c/fail20-pointer-addition-init1.c: New.
	* libbounds/testsuite/libbounds.c/pass28-pointer-addition-from-var-left.c: New.
	* libbounds/testsuite/libbounds.c/fail23-pointer-referent.c: New.
	* libbounds/testsuite/libbounds.c/pass14-pointer-subtraction2.c: New.
	* libbounds/testsuite/libbounds.c/fail42-struct-string.c: New.
	* libbounds/testsuite/libbounds.c/fail04-stack3darray.c: New.
	* libbounds/testsuite/libbounds.c/fail12-pointer-subtraction1.c: New.
	* libbounds/testsuite/libbounds.c/pass19-pointer-predecrement.c: New.
	* libbounds/testsuite/libbounds.c/pass24-pointer-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/fail32-pointer-address-addition.c: New.
	* libbounds/testsuite/libbounds.c/fail10-pointer.s: New.
	* libbounds/testsuite/libbounds.c/fail14-pointer-subtraction2.c: New.
	* libbounds/testsuite/libbounds.c/fail19-pointer-predecrement.c: New.
	* libbounds/testsuite/libbounds.c/fail26-pointer-oob.c: New.
	* libbounds/testsuite/libbounds.c/cfrags.exp: New.
	* libbounds/testsuite/libbounds.c/fail30-pointer-address.c: New.
	* libbounds/testsuite/libbounds.c/pass10-pointer.c: New.
	* libbounds/testsuite/libbounds.c/fail11-pointer-addition1.s: New.
	* libbounds/testsuite/libbounds.c/pass18-pointer-preincrement.c: New.
	* libbounds/testsuite/libbounds.c/pass27-pointer-addition-from-var-right.c: New.
	* libbounds/testsuite/libbounds.c/fail18-pointer-preincrement.c: New.
	* libbounds/testsuite/libbounds.c/pass46-struct-string-align.c: New.
	* libbounds/testsuite/libbounds.c/pass34-pointer-difference-oob.c: New.
	* libbounds/testsuite/libbounds.c/pass03-stack2darray.c: New.
	* libbounds/testsuite/libbounds.c/fail33-pointer-address-addition-from-var.c: New.
	* libbounds/testsuite/libbounds.c/pass31-pointer-address-from-var.c: New.
	* libbounds/testsuite/libbounds.c/pass13-pointer-addition2.c: New.
	* libbounds/testsuite/libbounds.c/fail03-stack2darray.c: New.
	* libbounds/testsuite/libbounds.c/fail17-pointer-addition-from-array.c: New.
	* libbounds/testsuite/libbounds.c/fail31-pointer-address-from-var.c: New.
	* libbounds/testsuite/libbounds.c/pass26-pointer-oob.c: New.
	* libbounds/testsuite/libbounds.c/pass30-pointer-address.c: New.
	* libbounds/testsuite/libbounds.c/pass02-stackarray.c: New.
	* libbounds/testsuite/libbounds.c/fail24-pointer-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/pass44-struct-nested.c: New.
	* libbounds/testsuite/libbounds.c/pass01-noop.c: New.
	* libbounds/testsuite/libbounds.c/fail05-stackarray-referent.c: New.
	* libbounds/testsuite/libbounds.c/pass06-stackarray-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/pass20-pointer-addition-init1.c: New.
	* libbounds/testsuite/libbounds.c/fail06-stackarray-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/pass17-pointer-addition-from-array.c: New.
	* libbounds/testsuite/libbounds.c/pass21-pointer-subtraction-init1.c: New.
	* libbounds/testsuite/libbounds.c/fail13-pointer-addition2.c: New.
	* libbounds/testsuite/libbounds.c/pass41-struct-array.c: New.
	* libbounds/testsuite/libbounds.c/fail28-pointer-addition-from-var-left.c: New.
	* libbounds/testsuite/libbounds.c/pass15-pointer-increment.c: New.
	* libbounds/testsuite/libbounds.c/fail06-stackarray-referent-loop.s: New.
	* libbounds/testsuite/libbounds.c/pass25-pointer-difference.c: New.
	* libbounds/testsuite/libbounds.c/pass05-stackarray-referent.c: New.
	* libbounds/testsuite/libbounds.c/fail44-struct-nested.c: New.
	* libbounds/testsuite/libbounds.c/fail35-function-params.c: New.
	* libbounds/testsuite/libbounds.c/pass40-struct.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass46-struct-string-align.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass16-pointer-decrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass34-pointer-difference-oob.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass11-pointer-addition1.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail40-struct.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail33-pointer-address-addition-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass31-pointer-address-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass42-struct-string.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass13-pointer-addition2.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail31-pointer-address-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail17-pointer-addition-from-array.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass26-pointer-oob.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass30-pointer-address.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail24-pointer-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass29-pointer-subtraction-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail15-pointer-increment.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail44.struct-nested.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail29-pointer-subtraction-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail10-pointer.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail27-pointer-addition-from-var-right.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass33-pointer-address-addition-from-var.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass23-pointer-referent.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass17-pointer-addition-from-array.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail46-struct-string-align.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass12-pointer-subtraction1.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail16-pointer-decrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass32-pointer-address-addition.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail11-pointer-addition1.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass28-pointer-addition-from-var-left.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass14-pointer-subtraction2.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail23-pointer-referent.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail42-struct-string.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail13-pointer-addition2.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail12-pointer-subtraction1.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass24-pointer-referent-loop.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass19-pointer-predecrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass41-struct-array.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail32-pointer-address-addition.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail28-pointer-addition-from-var-left.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail14-pointer-subtraction2.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass15-pointer-increment.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail41-struct-array.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail19-pointer-predecrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail26-pointer-oob.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail30-pointer-address.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass44.struct-nested.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass25-pointer-difference.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass10-pointer.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass18-pointer-preincrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass27-pointer-addition-from-var-right.c: New.
	* libbounds/testsuite/libbounds.c/heap/fail18-pointer-preincrement.c: New.
	* libbounds/testsuite/libbounds.c/heap/pass40-struct.c: New.
	* libbounds/testsuite/config/default.exp: New.
	* libbounds/testsuite/lib/boundsdg.exp: New.
	* libbounds/testsuite/lib/libbounds.exp: New.
	* libbounds/testsuite/Makefile.am: New.
	* libbounds/bounds-runtime.c: New.
	* libbounds/bounds-oob.h: New.
	* libbounds/bounds-splay-tree.c: New.
	* libbounds/config.h.in: New.
	* libbounds/bounds-runtime.h: New.
	* libbounds/configure.ac: New.
	* libbounds/bounds-splay-tree.h: New.
	* libbounds/autom4te.cache/output.1: New.
	* libbounds/autom4te.cache/traces.0: New.
	* libbounds/autom4te.cache/traces.1: New.
	* libbounds/autom4te.cache/requests: New.
	* libbounds/autom4te.cache/output.0: New.
	* libbounds/acinclude.m4: New.
	* libbounds/Makefile.am: New.
	* libbounds/aclocal.m4: New.
	* libbounds/bounds-hooks1.c: New.
	* libbounds/bounds-hooks2.c: New.

2007-08-03  Sebastian Pop  <sebastian.pop@inria.fr>

	* Makefile.in: Regenerated.
	* Makefile.def: Update cloog configure flags for detecting build dir
	of polylib.
	* cloog: Imported sources.
	* polylib: Imported sources.
	* contrib/get_graphite_libs.sh: Removed.

2007-05-10  Tobias Grosser  <tobi-grosser@web.de>
	    Sebastian Pop  <sebastian.pop@inria.fr>

	* configure.ac: Correct polylib and cloog library paths.
	* configure: Regenerate.

2007-04-14  Sebastian Pop  <sebastian.pop@inria.fr>

	* configure: Regenerated.
	* Makefile.in: Regenerated.
	* configure.ac: Detect polylib and cloog libraries.
	* Makefile.def: Declare dependences of polylib and cloog on other libs.
	* Makefile.tpl (POLYLIBLIBS, POLYLIBINC, CLOOGLIBS, CLOOGINC,
	HOST_POLYLIBLIBS, HOST_POLYLIBINC, HOST_CLOOGLIBS, HOST_CLOOGINC): New.
	* contrib/get_graphite_libs.sh: New.

